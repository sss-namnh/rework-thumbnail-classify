import torch
from tools.predicter import Predicter
from tools.config import Config

from tqdm import tqdm
import os
import time
import json
import argparse

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--config', default='./config/base.yaml', required=True)
    parser.add_argument('--save_out', default='.pred.json', required=False)

    args = parser.parse_args()
    Cfg = Config.load_config_from_file(args.config)

    models = torch.load(Cfg['checkpoints'])
    Predicter_ = Predicter(Cfg, models)

    if os.path.isfile(Cfg['path_test']):
        pred = Predicter_.predict(Cfg['path_test'])
        print(pred)
        exit()
    
    if os.path.isdir(Cfg['path_test']):
        list_res = []
        for img in tqdm(sorted(os.listdir(Cfg['path_test']))):
            imgs = os.path.join(Cfg['path_test'], img)
            pred = Predicter_.predict(imgs)
            print(pred)
            list_res.append(pred)

        with open(args.save_out, "w", encoding='utf8') as file_:
            json.dump(list_res, file_)



if __name__=="__main__":
    main()


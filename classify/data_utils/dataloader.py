from torch.utils.data import Dataset, DataLoader
from torchvision import datasets, transforms, models
from PIL import Image

import os
import json

class Datasets(Dataset):

    def __init__(self , Cfg, path_img = None, labels = None):
        
        self.Cfg = Cfg
        if labels:
            self.labels = json.load(open(labels))
            self.path = Cfg['path_img']
        else:
            self.labels = None
            self.path = path_img if path_img != None else self.Cfg['path_test']
        
        if os.path.isfile(self.path):
            self.images = [self.path.split('/')[0]]
        else:
            self.images = [x for x in os.listdir(self.path)]

        self.transform = transforms.Compose([
                transforms.Resize((224,224)),
                transforms.ToTensor(),
                transforms.Normalize((0.5,0.5,0.5), (0.5,0.5,0.5))
                ])

    def get_data(self, map):
        for img in self.labels:
            for label_ in self.Cfg['list_labels']:
                img['labels'][label_] = int(map[label_][img['labels'][label_]])
        return self.labels

    def __len__(self):
        return len(self.images)

    def __getitem__(self, idx):
        if os.path.isfile(self.path):
            self.img_path = self.path
        else:
            self.img_path = os.path.join(self.path, self.images[idx])
            
        image = Image.open(self.img_path).convert("RGB")
        single_img = self.transform(image)

        if self.labels == None:
            return {'images': single_img}

        label_ = None
        for img in self.labels:
            if img['images'] == self.images[idx]:
                label_ = img['labels']


        sample = {
            'images': single_img,
            'labels': label_
        }

        return sample


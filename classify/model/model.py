import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
import torchvision
from torchvision import datasets, transforms, models
import torchvision.models as models

class MultilabelClassifier(nn.Module):
    def __init__(self, n_source, n_shooting):
        super().__init__()
        self.resnet = models.resnet34(pretrained=True)
        self.model = nn.Sequential(*(list(self.resnet.children())[:-1]))

        self.source = nn.Sequential(
            nn.Dropout(p=0.2),
            nn.Linear(in_features=512, out_features=n_source)
        )

        self.shooting = nn.Sequential(
            nn.Dropout(p=0.2),
            nn.Linear(in_features=512, out_features=n_shooting)
        )

    def forward(self, x):
        x = self.model(x)
        x = torch.flatten(x, 1)

        return {
            'source': self.source(x),
            'shooting': self.shooting(x)
        }


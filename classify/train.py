import torch

from model.model import MultilabelClassifier
from data_utils.dataloader import *
from tools.trainer import Trainer
from tools.config import Config

import time
import json
import argparse

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--config', default='./config/base.yaml', required=True)

    args = parser.parse_args()
    Cfg = Config.load_config_from_file(args.config)

    train_data = Datasets(Cfg=Cfg, labels=Cfg['path_labels'])
    train_data.get_data(Cfg['maping_labels'])
    train_loader = DataLoader(train_data, 
                            batch_size=Cfg["batch_size"], 
                            shuffle=True, 
                            num_workers=Cfg["num_workers"], 
                            drop_last=True)

    Trainer_ = Trainer(Cfg)
    model, checkpoint_losses = Trainer_.train(train_loader)

    with open(Cfg['save_weight'],'wb') as open_file:
        torch.save(model, open_file)

if __name__=="__main__":
    main()
    





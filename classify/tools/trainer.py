import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader

from model.model import MultilabelClassifier
from data_utils.dataloader import Datasets


class Trainer():
  def __init__(self, Cfg, pretrained=False):
    self.Cfg = Cfg
    self.model = MultilabelClassifier(2,2).to(Cfg['device'])
    self.device = Cfg['device']

  def criterion(self, loss_func, outputs,labels):
    losses = 0
    for i, key in enumerate(outputs):
      losses += loss_func(outputs[key], labels[f'{key}'].to(self.device))
    return losses

  def train(self, train_loader):
    num_epochs = self.Cfg['epochs']
    losses = []
    checkpoint_losses = []

    optimizer = torch.optim.Adam(self.model.parameters(), lr=self.Cfg['lr_rate'])
    n_total_steps = len(train_loader)

    loss_func = nn.CrossEntropyLoss()

    for epoch in range(num_epochs):
      for i, pictures in enumerate(train_loader):
          images = pictures['images'].to(self.device)
          labels = pictures['labels']

          outputs = self.model(images)
          loss = self.criterion(loss_func,outputs, labels)
          losses.append(loss.item())

          optimizer.zero_grad()
          loss.backward()
          optimizer.step()

          if (i+1) % (int(n_total_steps/1)) == 0:
              checkpoint_loss = torch.tensor(losses).mean().item()
              checkpoint_losses.append(checkpoint_loss)
              print (f'Epoch [{epoch+1}/{num_epochs}], Step [{i+1}/{n_total_steps}], Loss: {checkpoint_loss:.4f}')
    return self.model, checkpoint_losses


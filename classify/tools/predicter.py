import torch
from torch.utils.data import Dataset, DataLoader
from data_utils.dataloader import Datasets

from tqdm import tqdm

class Predicter():
    def __init__(self, Cfg, model):
        self.device = Cfg['device']
        self.Cfg = Cfg
        self.model = model

    def predict(self, path_img):
        sample = Datasets(Cfg=self.Cfg, path_img=path_img)
        image = sample['images']['images']
        image = image.reshape(1,3,224,224)

        outputs = self.model(image.to(self.device))

        dict_res = {}
        for i, out in enumerate(outputs):
            _, predicted = torch.max(outputs[out],1)

            dict_res[out] = int(predicted[0])

        dict_res['source'] = 'screenshot' if dict_res['source'] else 'other'
        dict_res['shooting'] = 'product_only' if dict_res['shooting'] else 'model'
                
        return {"images": path_img.split("/")[-1],
                "labels": dict_res}
 



    